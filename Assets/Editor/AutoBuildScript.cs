using UnityEngine;
using UnityEditor;
class AutoBuildScript
{
	static string GetProjectName()
	{
		string[] s = Application.dataPath.Split('/');
		return s[s.Length - 2];
	}

     static void PerformBuild ()
     {
         string[] scenes = { "Assets/Terminal Hacker.unity" };
         EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTargetGroup.Standalone, BuildTarget.StandaloneWindows64);
         BuildPipeline.BuildPlayer(scenes, "Builds/Win64/" + GetProjectName() + ".exe", BuildTarget.StandaloneWindows64, BuildOptions.None);
     }
}
     
